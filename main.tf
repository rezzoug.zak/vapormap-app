terraform {
    required_version = ">= 0.14.0"
    required_providers {
        openstack = {
            source = "terraform-provider-openstack/openstack"
            version = "~> 1.35.0"
        }
    }
}

provider openstack {}

# ===
# Network
# ===

data "openstack_networking_network_v2" "ext_network" {
  name = "external"
}

data "openstack_networking_router_v2" "ext_router" {
  name = "K8S_external_router"
}

resource "openstack_networking_network_v2" "internal_net" {
  name = "internal_net"
  admin_state_up      = true
}

resource "openstack_networking_subnet_v2" "internal_sub" {
  name            = "internal_sub"
  network_id      = openstack_networking_network_v2.internal_net.id
  cidr            = "172.17.199.0/24"
  dns_nameservers = ["192.44.75.10", "192.108.115.2"]
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = data.openstack_networking_router_v2.ext_router.id
  subnet_id = openstack_networking_subnet_v2.internal_sub.id
}

# Create floating ip
resource "openstack_networking_floatingip_v2" "fip_http" {
  pool = data.openstack_networking_network_v2.ext_network.name
}
# ===
# Security Groups
# ===

# allow ingress ssh and http tcp protocol 
resource "openstack_networking_secgroup_v2" "secgroup_http_ssh" {
  name = "http_ssh"
}
resource "openstack_networking_secgroup_rule_v2" "sg_rules_ssh" {
direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_http_ssh.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sg_rules_http" {
direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_http_ssh.id}"
}

data "template_file" "user_data" {
  template = file("./scripts/add-ssh.yaml")
  vars = {
    user_ssh_pub_key = "ced_ed25519"
  }
}

# ===
# Instance
# ===

# ssh key pair to add to openstack project
resource "openstack_compute_keypair_v2" "user_key" {
  name       = "ced_ed25519"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPWahRNXaA94J7D8YzXFaA4cI4ehU1JsVLhXD3FfkEPxAQ0ZDg/N4Nv+cBY8o0/LN7Fo7s3MviWFeG2N7YDC5jy6iwinb3tbTisIy9AHZ6Z58y8Sr6uiCNexg5yPOtWyZbjrSX1yrs8JfYa0ywchN97LTfFmmOIrLxPYKucvnQXdSOQTToiOclz2jkD71hLw45FPbuq0fE7WQNheYcQm9evLKQt4gIxEIJVRnfHyZSEzYym46TD1RdPfUj4OA7CX3Ij870WXFU53nH5xBgm4sSSEyP7xAnLkAZTRw3O8QJv1+8UCWvZVHqygrRxs8JCFc0y7KU14w1vWz9botbOBd7LYxbfHWtPsqaHhBwIAKEiILxv6U1lE2YQBnNOeroDUa37zc/poKl99lAHf8V1Aruz8NWmNQVlyPVo7wIcAvZcOjaIhGR3XpfINpHdAJ5E3xqOQiaCGYUx93ax8f0SD7ms53nLUkcI5JZMKMuQTGU80xaU/IgwtO5ZZKQN81GiaU= user@SYS-Ubuntu20"
}

resource "openstack_compute_instance_v2" "std_srv" {
  name              = "std_ssrv-3"
  image_name        = "imta-docker"
  flavor_name       = "s20.medium"
  key_pair          = openstack_compute_keypair_v2.user_key.name
  security_groups   = ["http_ssh","default"]
  user_data       = data.template_file.user_data.rendered
  network {
      name = openstack_networking_network_v2.internal_net.name
  }
}
resource "openstack_compute_instance_v2" "Master" {
    name              = "Master"
    image_name        = "imta-docker"
    flavor_name       = "s20.medium"
    key_pair          = openstack_compute_keypair_v2.user_key.name
    security_groups   = ["http_ssh","default"]
    user_data       = data.template_file.user_data.rendered
    network {
        name = openstack_networking_network_v2.internal_net.name
    }
  }
  resource "openstack_compute_instance_v2" "Worker_1" {
  name              = "Worker 1"
  image_name        = "imta-docker"
  flavor_name       = "s20.medium"
  key_pair          = openstack_compute_keypair_v2.user_key.name
  security_groups   = ["http_ssh","default"]
  user_data       = data.template_file.user_data.rendered
  network {
      name = openstack_networking_network_v2.internal_net.name
  }
}
resource "openstack_compute_instance_v2" "Worker_2" {
    name              = "Worker 2"
    image_name        = "imta-docker"
    flavor_name       = "s20.medium"
    key_pair          = openstack_compute_keypair_v2.user_key.name
    security_groups   = ["http_ssh","default"]
    user_data       = data.template_file.user_data.rendered
    network {
        name = openstack_networking_network_v2.internal_net.name
    }
  }

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "http" {
  floating_ip = openstack_networking_floatingip_v2.fip_http.address
  instance_id = openstack_compute_instance_v2.std_srv.id
}
output "ext_IP" {
    description = "IP address of floating IP"
    value = openstack_networking_floatingip_v2.fip_http.address
}
