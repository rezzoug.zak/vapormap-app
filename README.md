


# Mastère Spécialisé Infrastructure Cloud et DevOps
## Compte rendu : Fil Rouge Partie 4
## Élève : Rezzoug Zakaria 
## Répertoire du projet : https://gitlab.com/rezzoug.zak/vapormap-app
## Première partie : initalisation des instances Docker Swarm sur Openstack
- Durant cette partie de fil rouge j'ai intialisé trois instance sur openstack pour déployé mon application apres ça j'ai eu a intialisé docker swarm sur une des instance, pour le master j'ai eu quelque soucis avec ça car j'avais fais le travail sur le poste de travail de la salle de tp et les instance etait sur openstack du coup pour les connecté entre eux ce n'etait pas une tache des plus simple car ils n'etaient dans le meme réseau alors j'ai directement opté pour l'utilisation que des instance openstack et que ça allait etre plus simple.
- Apres avoir fini d'initialisé mes instance manuelment j'ai pas utilisé terraform meme si par la suite je l'ai utilisé de mon coté pour tester j'ai eu quelque probleme mais c'etait que des probleme de syntax rien de tres graves j'ai lancé docker swarm dans les instance qui est une taches assez simple j'ai pas eu de probleme avec ça .
## Deuxième partie : Gitlab CI 
- Apres j'ai commencé a modifier mon gitlab ci pour pouvoir faire un déploiment automatique j'ai eu quelque probleme avec la syntaxe et j'ai eu besoin d'utilisé gitlab runner du ce fait je devais le configurer sur une de mes instances j'ai choisi celle du master j'ai eu quelque probleme pour le configurer j'ai tout indiqué dans la suite de ce fichier.

- J'avais eu surtout des probelems avec les proxy et les ports que je devais gérer qui n'etait pas evident mais a la fin j'ai pu lancer le runner et le joindre a gitlab.

- J'ai aussi fais de nombreux test des test de code static ( pour python) et des tests des dockerfile j'ai rajouter exit 0 car il comportait plusieurs erreurs et j'ai trouvé que ce n'etait pas necessaire de tout gerer.

- Apres il suffit juste de rajouter les lignes de deploiement dans le gitlab-ci.yml bien sur j'ai éssayé plusieur fois a cause des erreurs de syntax mais a la fin j'y suis arrivé.

```
Deploy_docker_compose:
  stage: deploy
  tags: 
    - swarm
  script: 
    - echo $CI_REGISTRY_PASSWORD |  docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker stack deploy --compose-file docker-compose.yml DeployOnSwarm
```

## Conculsion: 
Durent ce fil rouge pour m'a part j'ai pas eu beaucoup de probleme a géré il y'avait une documentation pour chaque élement qu'il suffisait juste de suivre les problemes que j'ai eu je l'ai indiqué mais aussi j'ai ajouté dans un fichier joint toutes les étapes que j'ai eu a faire pour lancé ma stack correctement. 

# Les etapes a suivre pour dépolyé depuis gitlab sur des instances openstack avec docker swarm
## 1- Créer les instances manuellement et les configurer ou bien utilisé terraform (pour ma part  dans cette partie j'ai utlisé terraform pour initialiser les instances seulement apres j'ai fais le le travail manuellement a l'interieur de ces instances )
### 1-1 les instances étaient avec une image imta-docker et de taille s10.medium.
## 2- Lancer docker swarm dans le master et le joindre avec les autres instances j'en ai utlisé 1 pour le master et 2 pour les worker.
pour la master :
```console
   docker swarm init
```
pour les worker : 
```console
   docker swarm join -token xxxxxxxxxxxx
```
## 3- Lancer le runner sur une des instances pour ma part j’ai choisi celle du master de docker swarm
### 3-1  Pour ce faire il faut installer le runner, tout d'abord il faut aller dans le projet apres sur le coté choisir paramettre et CI/CD et apres runner et suivre les étapes indiqués 
### 3-2 Apres il faut configurer le proxy du runner pour pouvoir l’utiliser :
```bash
    mkdir /etc/systemd/system/gitlab-runner.service.d 
```
### 3-3 Et a l’interieur crée le fichier http-proxy.conf avec les informations suivante :
```console
    nano /etc/systemd/system/gitlab-runner.service.d/http-proxy.conf
```
### 3-4 Et ajouter ça :
```
[Service]
Environment="HTTP_PROXY=http://proxy.enst-bretagne.fr:8080"
Environment="HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080"
```
### 3-5 Apres executer les commandes suivante 
```console
    systemctl daemon-reload
    sudo systemctl restart gitlab-runner
    systemctl show --property=Environment gitlab-runner
    gitlab-runner verify
```
#### Et pour plus d'informations vous pouvez aller sur https://docs.gitlab.com/runner/configuration/proxy.html
### 3-6 Une fois le runner lancé on va voir ça couleur en vert sur les paremetres CI/CD comme dans l'étape 4 normalement il va apparaître en vert.
## 6- Apres on pourra lancer notre pipline avec notre runner dans la partie déploy on doit ajouter le nom du runner dans tags( on doit avoir les build deja qui marche pour pouvoir faire ça ) dans l'exemple swarm et le nom du runner: 
```
Deploy_docker_compose:
  stage: Deploy
  tags: 
    - swarm
  script: 
    - echo $CI_REGISTRY_PASSWORD |  docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker stack deploy --compose-file docker-compose.yml DeployOnSwarm
```
## 7- Pour les tests des Dockerfile sur le projet vaportmap j'ai ajouter hadolint dans notre gitlab CI comme indiqué ci-dessus:
```
hadolint:
  stage: Analyse dockerfile
  before_script:
    - ''
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint Frontend/Dockerfile  || exit 0
    - hadolint Backend/python/Dockerfile || exit 0
```
j'ai rajouté le exit 0 vu le nombre de warnings qu'il y'avait
## 8- Pour les tests static du code python j'ai utilisé flake8 de python 
 ```
flake8:
   stage: Static Tests
   before_script:
     - pip install --no-cache-dir flake8 flake8-json
   script:
     - flake8 --max-line-length 120 --output-file=./output.json --format=json || exit 0
   artifacts:
     paths:
       - output.json
     name: vapormap_report
     expire_in: 1 week
```
## 7- Et la normalement si tout a été configurer correctement on a tout qui marche et si on vérifie la pipline sur gitlab normalement apres le commit ça va être en vert ce qui signifie que notre pipline a déployé la stack correctement.




